import React from 'react';
import AppRouter from './AppRouter';
import useStyles from './App.styles';

const App = () => {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <AppRouter />
    </div>
  );
};

export default App;
