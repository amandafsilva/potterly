import { createUseStyles } from 'react-jss';

const useStyles = createUseStyles({
  root: {
    height: '100vh',
    width: '100vw',
    fontFamily: 'Roboto',
  },
});

export default useStyles;
