import React from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import SortingCeremonyRoute from '../../routes/SortingCeremony/SortingCeremonyRoute';

const AppRouter = () => {
  return (
    <BrowserRouter>
      <Switch>
        <Route path="/sorting-ceremony" component={SortingCeremonyRoute} />
      </Switch>
    </BrowserRouter>
  );
};

export default AppRouter;
