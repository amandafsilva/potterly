import React from 'react';
import { shallow } from 'enzyme';
import AppRouter from './AppRouter';
import SortingCeremonyRoute from '../../routes/SortingCeremony/SortingCeremonyRoute';

describe('AppRouter', () => {
  it('should render without crashing', () => {
    // Given
    const wrapper = shallow(<AppRouter />);

    // Then
    expect(wrapper.exists()).toBeTruthy();
  });

  it('should render the correct routes', () => {
    // Given
    const wrapper = shallow(<AppRouter />);
    const expectedRoutes = [
      {
        path: '/sorting-ceremony',
        component: SortingCeremonyRoute,
      },
    ];

    // Then
    expect(wrapper.find('BrowserRouter')).toHaveLength(1);
    expect(wrapper.find('Switch')).toHaveLength(1);

    const routeWrapper = wrapper.find('Route');
    expect(routeWrapper).toHaveLength(expectedRoutes.length);
    routeWrapper.forEach((route, index) => {
      expect(route.props()).toStrictEqual(expectedRoutes[index]);
    });
  });
});
