import { createUseStyles } from 'react-jss';

const useStyles = createUseStyles({
  container: {
    height: '100%',
    width: '100%',
    backgroundColor: '#232323',
  },
});

export default useStyles;
