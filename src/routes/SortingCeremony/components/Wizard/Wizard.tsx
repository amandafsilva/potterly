import React from 'react';
import useStyles from './Wizard.styles';

const Wizard = () => {
  const classes = useStyles();

  return (
    <div className={classes.container}>
      <div className={classes.avatar}>
        <div className={classes.wizard}>
          <div className={classes.hair}></div>
          <div className={classes.leftEar}></div>
          <div className={classes.rightEar}></div>
          <div className={classes.face}></div>
          <div className={classes.bangs}></div>
          <div className={classes.leftEyebrow}></div>
          <div className={classes.leftEye}></div>
          <div className={classes.rightEyebrow}></div>
          <div className={classes.rightEye}></div>
          <div className={classes.nose}></div>
          <div className={classes.mouth}>
            <div className={classes.tongue}></div>
          </div>
          <div className={classes.robe}></div>
          <div className={classes.robeDetails}></div>
          <div className={classes.shirt}></div>
          <div className={classes.tie}></div>
        </div>
      </div>
    </div>
  );
};

export default Wizard;
