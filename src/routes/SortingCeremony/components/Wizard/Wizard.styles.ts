import { createUseStyles } from 'react-jss';

const beige20 = '#F0BEAF';
const black30 = '#1e1e1e';
const brown80 = '#4d2d1a';
const grey10 = '#E8E9EB';
const white = '#ffffff';
const tempRed = '#5c0000';

const useStyles = createUseStyles({
  container: {
    position: 'absolute',
    margin: 'auto',
    top: 0,
    bottom: 0,
    right: 0,
    left: 0,
    width: 350,
    height: 350,
  },
  avatar: {
    position: 'relative',
    width: 350,
    height: 350,
    border: `2px solid ${white}`,
    overflow: 'hidden',
    background: grey10,
    borderRadius: '100%',
  },
  wizard: {
    position: 'relative',
  },
  hair: {
    position: 'absolute',
    zIndex: 3,
    top: 35,
    left: 68,
    width: 210,
    height: 300,
    borderRadius: '100px 100px 0 0',
    background: brown80,
  },
  robeHood: {
    position: 'absolute',
    zIndex: 3,
    top: 35,
    left: 68,
    width: 210,
    height: 230,
    borderRadius: '100px',
    background: black30,
  },
  robeHoodDetails: {
    position: 'absolute',
    zIndex: 3,
    top: 50,
    left: 80,
    width: 185,
    height: 210,
    borderRadius: '100px',
    background: tempRed,
  },
  bangs: {
    position: 'absolute',
    zIndex: 20,
    top: 40,
    left: 125,
    width: 120,
    height: 75,
    background: brown80,
    borderRadius: '50% 50% 30px 30px',
  },
  leftEar: {
    position: 'absolute',
    zIndex: 4,
    top: 150,
    left: 75,
    width: 30,
    height: 25,
    borderRadius: '100%',
    background: beige20,
  },
  rightEar: {
    position: 'absolute',
    zIndex: 4,
    top: 150,
    right: 75,
    width: 30,
    height: 25,
    borderRadius: '100%',
    background: beige20,
  },
  face: {
    borderRadius: '100%',
    zIndex: 20,
    top: 60,
    left: 88,
    width: 170,
    height: 195,
    background: beige20,
    position: 'absolute',
  },
  leftEyebrow: {
    position: 'absolute',
    zIndex: 300,
    top: 125,
    left: 120,
    width: 25,
    height: 4,
    background: brown80,
  },
  rightEyebrow: {
    position: 'absolute',
    zIndex: 300,
    top: 125,
    right: 120,
    width: 25,
    height: 4,
    background: brown80,
  },
  leftEye: {
    position: 'absolute',
    zIndex: 30,
    top: 140,
    left: 125,
    width: 15,
    height: 20,
    background: black30,
    borderRadius: '100%',
  },
  rightEye: {
    position: 'absolute',
    zIndex: 30,
    top: 140,
    right: 125,
    width: 15,
    height: 20,
    background: black30,
    borderRadius: '100%',
  },
  nose: {
    zIndex: 20,
    top: 150,
    left: 170,
    width: 9,
    height: 25,
    background: '#DB9B99',
    borderRadius: 100,
    position: 'absolute',
  },
  mouth: {
    position: 'absolute',
    zIndex: 20,
    top: 190,
    left: 140,
    width: 70,
    height: 40,
    borderRadius: '0 0 100px 100px',
    background: black30,
    overflow: 'hidden',
  },
  tongue: {
    position: 'absolute',
    top: 20,
    left: 20,
    width: 40,
    height: 23,
    background: '#F6828C',
    borderRadius: '100%',
  },
  robe: {
    position: 'absolute',
    zIndex: 10,
    top: 250,
    left: 90,
    width: 175,
    height: 150,
    background: black30,
    borderRadius: '100px',
  },
  robeDetails: {
    position: 'absolute',
    zIndex: 11,
    top: 250,
    left: 130,
    width: 90,
    height: 150,
    background: tempRed,
    borderRadius: '70% 80% 80% 70%/25% 25% 130% 130%',
  },
  shirt: {
    position: 'absolute',
    zIndex: 12,
    top: 250,
    left: 145,
    width: 60,
    height: 150,
    background: white,
    borderRadius: '70% 80% 80% 70%/25% 25% 130% 130%',
  },
  tie: {
    position: 'absolute',
    zIndex: 13,
    top: 253,
    left: 160,
    width: 30,
    height: 150,
    background: black30,
    borderRadius: '90% 60% 2px 2px/130% 130% 25% 25%',
    '&:before': {
      content: '""',
      position: 'absolute',
      width: 30,
      height: 25,
      borderRadius: '50% 50% 10px 10px / 25% 25% 130% 130%',
      margin: 'auto',
      top: 0,
      left: 0,
      right: 0,
      background: black30,
      boxShadow: '0 2px 3px rgba(0, 0, 0, 0.2)',
    },
  },
});

export default useStyles;
