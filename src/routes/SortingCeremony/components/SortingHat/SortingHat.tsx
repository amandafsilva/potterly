import React from 'react';
import useStyles from './SortingHat.styles';

const SortingHat = () => {
  const classes = useStyles();

  return (
    <div className={classes.container}>
      <div className={classes.base}></div>
      <div className={classes.top}></div>
      <div className={classes.leftEye}></div>
      <div className={classes.rightEye}></div>
      <div className={classes.mouth}></div>
    </div>
  );
};

export default SortingHat;
