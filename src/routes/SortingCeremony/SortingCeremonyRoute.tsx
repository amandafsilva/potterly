import React from 'react';
import Wizard from './components/Wizard/Wizard';
import useStyles from './SortingCeremonyRoute.styles';
import SortingHat from './components/SortingHat/SortingHat';

const SortingCeremonyRoute = () => {
  const classes = useStyles();

  return (
    <div className={classes.container}>
      <Wizard />
      <SortingHat />
    </div>
  );
};

export default SortingCeremonyRoute;
